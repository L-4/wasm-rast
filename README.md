## Setup

See [WABT readme](./wabt/README.md) and [Binaryen readme](./bin/README.md)
Only WABT is required.

`cpp` (C preprocessor) and `sed` must be in environment.

Run `./build.sh` to build.
Run a web server (such as `python -m http.server` or `http-server`) in this directory and navigate to `localhost` to see output.
