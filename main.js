const main = async () =>
{
    const canvas = document.getElementById("canvas");
    const fit_canvas = () =>
    {
        const ms = Math.min(window.innerWidth, window.innerHeight);
        canvas.style.width = `${ms}px`;
        canvas.style.height = `${ms}px`;
    };

    fit_canvas();
    window.addEventListener("resize", fit_canvas);

    const ctx = canvas.getContext("2d");
    let data_view;

    const upload_data = () =>
    {
        ctx.putImageData(new ImageData(data_view, 256, 256), 0, 0);
    };
    let e;

    // Backbuffer takes two pages, one for statics, one for initial heap allocations
    const mem = new WebAssembly.Memory({ initial: 6 });

    const warn = (line) => console.warn(`WASM warning on line ${line} of ...some file`);
    const tell = (what) => console.log(`WASM wants to tell you that: ${what} (0x${what.toString(16)})`);
    const putc = (c) => console.log(`putc "${String.fromCharCode(c)}"`);
    const put_i = (x) => console.log(`put_i(${x})`);
    const put_ii = (x, y) => console.log(`(${x}, ${y})`);
    const put_f = (x) => console.log(`put_f(${x})`);
    const put_ff = (x, y) => console.log(`put_ff(${x}, ${y}): len: ${Math.sqrt(x * x + y * y)}`);

    const vtx_origins = [[86, 110], [180, 110], [180, 138], [86, 138]];
    const vtx_frequencies = [3, 6, 8, 9];
    const vtx_magnitudes = [3, 3, 3, 3];

    const looping = true;

    const frame_avg_count = 16; // We do a little profilin'
    const frame_times = Array(frame_avg_count);
    let frame_idx = 0;

    // const constant_triangle_data = [
    //     (150 + Math.cos(2 * Math.PI * 0) * 50),
    //     (150 + Math.sin(2 * Math.PI * 0) * 50),
    //     (150 + Math.cos(2 * Math.PI * 2 / 3) * 50),
    //     (150 + Math.sin(2 * Math.PI * 2 / 3) * 50),
    //     (150 + Math.cos(2 * Math.PI * 1 / 3) * 50),
    //     (150 + Math.sin(2 * Math.PI * 1 / 3) * 50),
    //     0xffbf5836,
    // ];

    const draw = () =>
    {
        const now = performance.now();

        // note: agbr
        // bg 0x32a852
        // fg 0x3658bf

        e.clear(0xff52a832);
        const nowf = now * 0.001 * 0.3;

        // for (let i = 0; i < 1000; ++i)
        //     e.draw_triangle_old.apply(null, constant_triangle_data);

        const vertices = [];
        for (let i = 0; i < vtx_origins.length; ++i)
        {
            let [x, y] = vtx_origins[i];
            const freq = vtx_frequencies[i];
            const mag = vtx_magnitudes[i];
            let n = nowf * freq;
            x += Math.cos(n) * mag;
            y += Math.sin(n) * mag;

            vertices.push(x, y);
        }

        const triangle_data = [
            (150 + Math.cos(nowf + 2 * Math.PI * 0) * 50),
            (250 + Math.sin(nowf + 2 * Math.PI * 0) * 50),
            (150 + Math.cos(nowf + 2 * Math.PI * 2 / 3) * 50),
            (250 + Math.sin(nowf + 2 * Math.PI * 2 / 3) * 50),
            (150 + Math.cos(nowf + 2 * Math.PI * 1 / 3) * 50),
            (250 + Math.sin(nowf + 2 * Math.PI * 1 / 3) * 50),
            0xff20fc03,
        ];

        e.draw_triangle.apply(null, triangle_data);

        e.draw_triangle(vertices[0], vertices[1], vertices[4], vertices[5], vertices[2], vertices[3], 0xffbf5800 | (Math.sin(nowf) * 0xff));
        e.draw_triangle(vertices[0], vertices[1], vertices[6], vertices[7], vertices[4], vertices[5], 0xffbf5800 | (Math.sin(nowf + 10) * 0xff));

        // e.draw_triangle(128, 64, 64, 170, 200, 210, 0xffbf5836);

        e.draw();

        upload_data();
        frame_times[frame_idx] = performance.now() - now;
        ++frame_idx;

        if (frame_idx === frame_avg_count)
        {
            let total = 0;
            for (let i = 0; i < frame_avg_count; ++i)
                total += frame_times[i];

            // console.log(`last ${frame_avg_count} frames took an average of ${total / frame_avg_count}ms`);
            frame_idx = 0;
        }

        if (looping) requestAnimationFrame(draw);
    };

    const interface = {
        link: { backbuffer: mem },
        debug: { put_i, put_ii, putc, warn, tell, put_f, put_ff },
    };

    const response = await fetch("bin/main.wasm");
    const { module, instance } = await WebAssembly.instantiate(await response.arrayBuffer(), interface);

    e = instance.exports;
    window.e = e;
    e.init();

    // buf = e.alloc_last(4 * 4 * 16);
    // const [o, l] = e.get_alloc_view(buf);
    // buf_view = new DataView(instance.exports.backbuffer.buffer, o, l);
    // for (let i = 0; i < 16; ++i)
    // {
    //     buf_view.setInt32(i * 16 + 0, 10 + i * 10, true);
    //     buf_view.setInt32(i * 16 + 4, 13, true);
    //     buf_view.setInt32(i * 16 + 8, i * 10 + 50, true);
    //     buf_view.setInt32(i * 16 + 12, 470, true);
    // }

    data_view = new Uint8ClampedArray(instance.exports.backbuffer.buffer, 0, 256 * 256 * 4);
    draw_line = instance.exports.draw_line;
    draw();

    if (!looping)
        document.addEventListener("keydown", draw);
};

window.addEventListener("DOMContentLoaded", main);
