#!/bin/bash

mkdir -p temp
cpp -E $@ src/module.wat | sed -r 's/#/;;/' | sed -r 's/%(\w+)/(local.get $\1)/g' > temp/module.wat
