#pragma once

;; STACK VS PTR
;; I profiled the difference between passing a matrix as a pointer vs on the stack.
;; See the $m4_mul_v4_stack/ptr procedures below.
;; mat4 * vec4 5000000 times
;;                                  | Firefox | Chrome
;; All values on stack              | 212ms   | 84ms
;; Values in ptr (cache coherent)   | 230ms   | 44ms
;; Values in ptr (cache incoherent) | 300ms   | 89ms

;; Conclusions:
;; - Use pointers instead of the stack, in the best case the stack doesnt lose
;;      any time, but is much harder to read/write.
;; - Cache coherency seems to matter much less on firefox (but obviously is very)
;;      important on chrome.
;; - Using pointers also mades SIMD easier.

(func $abs (param $val i32) (result i32)
    (local $mask i32)

    local.get $val
    i32.const 31
    i32.shr_s ;; stack[0] $val >> 31

    local.tee $mask ;; $mask = stack[0]
    local.get $val
    i32.add

    local.get $mask
    i32.xor
)

(func $i32_cross
    (param $x0 i32) (param $y0 i32) (param $z0 i32)
    (param $x1 i32) (param $y1 i32) (param $z1 i32)
    (result i32) (result i32) (result i32)

    (i32.sub (i32.mul %y0 %z1) (i32.mul %z0 %y1))
    (i32.sub (i32.mul %z0 %x1) (i32.mul %x0 %z1))
    (i32.sub (i32.mul %x0 %y1) (i32.mul %y0 %x1))
)

(func $f32_cross
    (param $x0 f32) (param $y0 f32) (param $z0 f32)
    (param $x1 f32) (param $y1 f32) (param $z1 f32)
    (result f32) (result f32) (result f32)

    (f32.sub (f32.mul %y0 %z1) (f32.mul %z0 %y1))
    (f32.sub (f32.mul %z0 %x1) (f32.mul %x0 %z1))
    (f32.sub (f32.mul %x0 %y1) (f32.mul %y0 %x1))
)

(func $i32_barycentric
    (param $x0 i32) (param $y0 i32)
    (param $x1 i32) (param $y1 i32)
    (param $x2 i32) (param $y2 i32)
    (param $px i32) (param $py i32)
    (result f32) (result f32)

    (local $x f32) (local $y f32) (local $z f32)

    (call $i32_cross
        (i32.sub %x2 %px) (i32.sub %x1 %px) (i32.sub %x0 %px)
        (i32.sub %y2 %py) (i32.sub %y1 %py) (i32.sub %y0 %py)
    )
    ;; This part is easier to write without S-expressions. stack is: x, y, z (all ints)

    f32.convert_i32_s
    local.set $z
    f32.convert_i32_s
    local.get $z
    f32.div
    local.set $y
    f32.convert_i32_s
    local.get $z
    f32.div
    local.get $y
)

(func $f32_barycentric
    (param $x0 f32) (param $y0 f32)
    (param $x1 f32) (param $y1 f32)
    (param $x2 f32) (param $y2 f32)
    (param $px f32) (param $py f32)
    (result f32) (result f32)

    (local $y f32) (local $z f32)

    (call $f32_cross
        (f32.sub %x2 %x0) (f32.sub %x1 %x0) (f32.sub %x0 %px)
        (f32.sub %y2 %y0) (f32.sub %y1 %y0) (f32.sub %y0 %py)
    )

    local.tee $z
    f32.div
    local.set $y
    local.get $z
    f32.div
    local.get $y
)

(func $rgb_from_floats
    (param $r f32) (param $g f32) (param $b f32)
    (result i32)
    (local $t i32)

    ;; (local.set $r (f32.max (f32.min %r (f32.const 1)) (f32.const 0)))
    ;; (local.set $g (f32.max (f32.min %g (f32.const 1)) (f32.const 0)))
    ;; (local.set $b (f32.max (f32.min %b (f32.const 1)) (f32.const 0)))

    (i32.const 0xff000000)
    (i32.or (i32.trunc_f32_s (f32.mul %r (f32.const 255))))
    (i32.or (i32.shl (i32.trunc_f32_s (f32.mul %g (f32.const 255))) (i32.const 8)))
    (i32.or (i32.shl (i32.trunc_f32_s (f32.mul %b (f32.const 255))) (i32.const 16)))
)

(func $m4_mul_v4_stack
    (param $m00 f32) (param $m10 f32) (param $m20 f32) (param $m30 f32)
    (param $m01 f32) (param $m11 f32) (param $m21 f32) (param $m31 f32)
    (param $m02 f32) (param $m12 f32) (param $m22 f32) (param $m32 f32)
    (param $m03 f32) (param $m13 f32) (param $m23 f32) (param $m33 f32)

    (param $v0 f32) (param $v1 f32) (param $v2 f32) (param $v3 f32)
    (result f32) (result f32) (result f32) (result f32)

    (f32.add (f32.add (f32.add (f32.mul %m00 %v0) (f32.mul %m10 %v1)) (f32.mul %m20 %v2) (f32.mul %m30 %v3)))
    (f32.add (f32.add (f32.add (f32.mul %m01 %v0) (f32.mul %m11 %v1)) (f32.mul %m21 %v2) (f32.mul %m31 %v3)))
    (f32.add (f32.add (f32.add (f32.mul %m02 %v0) (f32.mul %m12 %v1)) (f32.mul %m22 %v2) (f32.mul %m32 %v3)))
    (f32.add (f32.add (f32.add (f32.mul %m03 %v0) (f32.mul %m13 %v1)) (f32.mul %m23 %v2) (f32.mul %m33 %v3)))
)

(func $m4_mul_v4_ptr
    (param $m i32)

    (param $v0 f32) (param $v1 f32) (param $v2 f32) (param $v3 f32)
    (result f32) (result f32) (result f32) (result f32)

    (f32.add (f32.add (f32.add (f32.mul (f32.load offset=0  %m) %v0) (f32.mul (f32.load offset=16 %m) %v1)) (f32.mul (f32.load offset=32 %m) %v2) (f32.mul (f32.load offset=48 %m) %v3)))
    (f32.add (f32.add (f32.add (f32.mul (f32.load offset=4  %m) %v0) (f32.mul (f32.load offset=20 %m) %v1)) (f32.mul (f32.load offset=36 %m) %v2) (f32.mul (f32.load offset=52 %m) %v3)))
    (f32.add (f32.add (f32.add (f32.mul (f32.load offset=8  %m) %v0) (f32.mul (f32.load offset=24 %m) %v1)) (f32.mul (f32.load offset=40 %m) %v2) (f32.mul (f32.load offset=56 %m) %v3)))
    (f32.add (f32.add (f32.add (f32.mul (f32.load offset=12 %m) %v0) (f32.mul (f32.load offset=28 %m) %v1)) (f32.mul (f32.load offset=44 %m) %v2) (f32.mul (f32.load offset=60 %m) %v3)))
)
