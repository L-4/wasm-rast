(module
#ifdef DEBUG
    (import "debug" "warn" (func $warn (param i32)))
    (import "debug" "tell" (func $tell (param i32)))
    (import "debug" "putc" (func $putc (param i32)))
    (import "debug" "put_i" (func $put_i (param i32)))
    (import "debug" "put_ii" (func $put_ii (param i32) (param i32)))
    (import "debug" "put_f" (func $put_f (param f32)))
    (import "debug" "put_ff" (func $put_ff (param f32) (param f32)))
#endif
    (memory (export "backbuffer") 6)

    #include "main.wat"
)
