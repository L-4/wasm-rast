#pragma once

#include "helpers.wat"
#include "math.wat"
#include "draw.wat"
#include "memory.wat"

(func (export "init")
    call $init_dummy_alloc
)

#define test_tri(Y0, Y1, XM, XL, XR) (call $fill_partial_triangle (i32.const Y0) (i32.const Y1) (i32.const XM) (i32.const XL) (i32.const XR) (i32.const 0xffffffff))

(func (export "draw")
    (call $blit_string_null_terminated (i32.const 96) (i32.const 120) (i32.const SM_TEST_STR) (i32.const 0xff00eaff))

    ;; test_tri(30, 60, 50, 20, 60) ;; y0 < y1, xl < xm < xr
    ;; test_tri(60, 30, 50, 20, 60) ;; y0 > y1, xl < xm < xr
    ;; test_tri(30, 60, 20, 50, 60) ;; y0 < y1, xm < xl < xr
    ;; test_tri(60, 30, 20, 50, 60) ;; y0 > y1, xm < xl < xr
    ;; test_tri(30, 60, 20, 60, 50) ;; y0 < y1, xm < xr < xl
)

(func (export "test") (param $v i32) (result i32)
    local.get $v
    i32.const 24
    i32.shr_s
)
