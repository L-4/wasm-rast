#pragma once

#include "helpers.wat"
#include "memory.wat"
#include "test_font.wat"

;; Rasterization, software rendering
;; https://www.scratchapixel.com/lessons/3d-basic-rendering/rasterization-practical-implementation/rasterization-stage
;; https://www.digipen.edu/sites/default/files/public/docs/theses/salem-haykal-digipen-master-of-science-in-computer-science-thesis-an-optimized-triangle-rasterizer.pdf

;; Note to self - in the brute force triangle rasterization code, we use an sdf function
;; for intersection testing. This means that we in some sense know by how far we missed the
;; triangle if we did.
;; Could this value be used to skip forward in the loop, if we used a space (grid) filling curve
;; with some amount of spatial contiguity?
;; See for instance the E-Curve at 1.3-C
;; https://jjj.de/pub/arndt-curve-search-arxiv-2018.pdf

;; WASM instruction/spec reference
;; WASM cheat-sheet:                 https://github.com/sunfishcode/wasm-reference-manual/blob/master/WebAssembly.md
;; SIMD instruction reference:       https://github.com/WebAssembly/simd/blob/main/proposals/simd/SIMD.md

;; Interestingly, x << 2 is slower than x * 4. Why?
;; This used to be two separate calls - $get_pixel_ofs and $set_pixel.
;; Inlining them made drawing 500 triangles go from taking 70ms to 30ms
#define set_pixel(X, Y, V) \
    (i32.store \
        (i32.add \
            (i32.mul (i32.const 1024) (local.get Y)) \
            (i32.mul (i32.const 4) (local.get X))) \
        (local.get V))

(func $fill_rect_region (param $x1 i32) (param $y1 i32) (param $x2 i32) (param $y2 i32) (param $v i32)
    (local $xv i32)
    (local $yv i32)

    (local.set $xv (local.get $x1))
    (loop
        (local.set $yv (local.get $y1))
        (loop
            set_pixel($xv, $yv, $v)
            i32_increment_local($yv)
            (br_if 0 (i32.lt_s (local.get $yv) (local.get $y2)))
        )
        i32_increment_local($xv)
        (br_if 0 (i32.lt_s (local.get $xv) (local.get $x2)))
    )
)

(func $blit_char (param $xo i32) (param $yo i32) (param $char i32) (param $c i32)
    (local $char_bits i64)
    (local $i i32)
    (local $x i32)
    (local $y i32)

    ;; We do not have separate sprites for lowercase letters, so translate uppercase to lowercase.
    ;; Note that this will also translate {|}~ to [\]^
    (if (i32.ge_s (local.get $char) (i32.const 0x60)) (then
        (local.set $char (i32.and (local.get $char) (i32.const 0xdf)))
    ))

    ;; Then we adjust the idx such that ascii 32 starts at zero
    (local.set $char (i32.sub (local.get $char) (i32.const 32)))

    (local.set $char_bits
        (i64.load
            (i32.add
                (i32.mul
                    (local.get $char)
                    (i32.const 8))
                (i32.const SM_TEST_FONT))))

    (local.set $i (i32.const 0))
    (loop
        (if
            (i64.gt_u (i64.and
                (local.get $char_bits)
                (i64.shl (i64.const 1) (i64.extend_i32_u (local.get $i)))) (i64.const 0))
            (then
                ;; // @todo: We dont need immediates, only required due to inlined macro
                (local.set $x (i32.add (local.get $xo) (i32.rem_s (local.get $i) (i32.const 8))))
                (local.set $y (i32.add (local.get $yo) (i32.div_s (local.get $i) (i32.const 8))))
                set_pixel($x, $y, $c)
            )
        )

        (local.set $i (i32.add (local.get $i) (i32.const 1)))
        (br_if 0 (i32.lt_s (local.get $i) (i32.const 64)))
    )
)

(func $blit_string_null_terminated (param $x i32) (param $y i32) (param $ptr i32) (param $c i32)
    (local $i i32)
    (local $char i32)
    (local.set $i (i32.const 0))

    (block $break
        (loop
            (local.set $char (i32.load8_u (i32.add (local.get $ptr) (local.get $i))))
            (br_if $break (i32.eq (local.get $char) (i32.const 0)))

            (call $blit_char
                (i32.add (local.get $x) (i32.mul (local.get $i) (i32.const 8)))
                (local.get $y)
                (local.get $char)
                (local.get $c)
            )

            (local.set $i (i32.add (local.get $i) (i32.const 1)))
            br 0
        )
    )
)

;; The old implementation is below, for future reference.
;; Clearing 100 times used to take 95ms, now it takes just 30ms.
;; If we also have SIMD available, then this goes down to 13ms!
(export "clear" (func $clear))
#ifdef WITH_SIMD
(func $clear (param $color i32)
    (local $color_x4 v128)
    (local $ofs i32)
    (local.set $color_x4 (i32x4.splat (local.get $color)))
    (local.set $ofs (i32.const 0))

    (loop
        (v128.store (local.get $ofs) (local.get $color_x4))

        (local.set $ofs (i32.add (local.get $ofs) (i32.const 16)))
        (br_if 0 (i32.lt_s (local.get $ofs) (i32.const 0x40000))) ;; The backbuffer is conveniently exactly four pages
    )
)
#else
(func $clear (param $color i32)
    (local $ofs i32)
    (local.set $ofs (i32.const 0))

    (loop
        (i32.store (local.get $ofs) (local.get $color))

        (local.set $ofs (i32.add (local.get $ofs) (i32.const 4)))
        (br_if 0 (i32.lt_s (local.get $ofs) (i32.const 0x40000))) ;; The backbuffer is conveniently exactly four pages
    )
)
#endif

;; (func $clear (param $color i32)
;;     (call $fill_rect_region (i32.const 0) (i32.const 0) (i32.const 256) (i32.const 256) (local.get $color))
;; )

(export "draw_line" (func $line))
(func $line (param $x0 i32) (param $y0 i32) (param $x1 i32) (param $y1 i32) (param $c i32)
    (local $steep i32)
    (local $dx i32)
    (local $dy i32)

    (local $derror2 i32)
    (local $error2 i32)
    (local $dx2 i32)

    (local $y i32)
    (local $x i32)

    (local $y_sign i32)


    (local.set $steep (i32.const 0)) ;; @todo
    (if (i32.lt_s ;; if x dist < y dist, then transpose
            (call $abs (i32.sub (local.get $x0) (local.get $x1)))
            (call $abs (i32.sub (local.get $y0) (local.get $y1)))
        )
        (then
            swap_local($x0, $y0)
            swap_local($x1, $y1)
            (local.set $steep (i32.const 1))
        )
    )

    ;; x now represents the "greater" axis
    ;; make sure that x0 is on the left (/top)
    (if (i32.gt_s (local.get $x0) (local.get $x1))
        (then
            swap_local($x0, $x1)
            swap_local($y0, $y1)
        )
    )

    (local.set $dx (i32.sub (local.get $x1) (local.get $x0)))
    (local.set $dy (i32.sub (local.get $y1) (local.get $y0)))

    (local.set $dx2 (i32.mul (local.get $dx) (i32.const 2)))
    (local.set $derror2 (i32.mul (call $abs (local.get $dy)) (i32.const 2)))
    (local.set $error2 (i32.const 0))

    (local.set $y (local.get $y0))
    (local.set $x (local.get $x0))

    (local.set $y_sign sign_from($y0, $y1))

    (loop
        (if (local.get $steep)
            (then set_pixel($y, $x, $c))
            (else set_pixel($x, $y, $c))
        )

        increment_by($error2, $derror2)

        (if (i32.gt_s (local.get $error2) (local.get $dx))
            (then
                increment_by($y, $y_sign)
                decrement_by($error2, $dx2)
            )
        )

        (local.set $x (i32.add (local.get $x) (i32.const 1)))
        (br_if 0 (i32.le_s (local.get $x) (local.get $x1)))
    )
)

(export "draw_array" (func $draw_array))
(func $draw_array (param $array i32) (param $color i32) (param $count i32)
    (local $ptr i32)
    (local $i i32)

    (local.set $ptr (i32.add (local.get $array) (i32.const MH_DATA_OFS)))
    (local.set $i (i32.const 0))

    (loop
        mem_load_base_ofs($ptr, 0)
        mem_load_base_ofs($ptr, 4)
        mem_load_base_ofs($ptr, 8)
        mem_load_base_ofs($ptr, 12)
        local.get $color

        call $line

        (local.set $ptr (i32.add (local.get $ptr) (i32.const 16)))
        (local.set $i (i32.add (local.get $i) (i32.const 1)))
        (br_if 0 (i32.le_s (local.get $i) (local.get $count)))
    )
)

;; This procedure currently takes 30ms to do the triangle test case
(export "draw_triangle_lines_1" (func $draw_triangle_n))
(func $draw_triangle_n
    (param $x0 i32) (param $y0 i32)
    (param $x1 i32) (param $y1 i32)
    (param $x2 i32) (param $y2 i32)
    (param $color i32)

    (local $long_slope f32)

    (if (i32.gt_s (local.get $y0) (local.get $y1))
        (then
            swap_local($x0, $x1)
            swap_local($y0, $y1)
        )
    )

    (if (i32.gt_s (local.get $y0) (local.get $y2))
        (then
            swap_local($x0, $x2)
            swap_local($y0, $y2)
        )
    )

    (if (i32.gt_s (local.get $y1) (local.get $y2))
        (then
            swap_local($x1, $x2)
            swap_local($y1, $y2)
        )
    )

    ;; y0 < y1 < y2
    ;; x is in arbitrary order
    ;; @todo: Check for all degenerate triangles.

    ;; 1. Skip triangles we dont care about

    ;; All points are the same on an axis
    (if (i32.and (i32.eq %y0 %y1) (i32.eq %y1 %y2)) (then return))
    (if (i32.and (i32.eq %x0 %x1) (i32.eq %x1 %x2)) (then return))

    ;; Two points in the same position
    (if (i32.and (i32.eq %x0 %x1) (i32.eq %y0 %y1)) (then return))
    (if (i32.and (i32.eq %x1 %x2) (i32.eq %y1 %y2)) (then return))
    (if (i32.and (i32.eq %x2 %x0) (i32.eq %y2 %y0)) (then return))

    ;; Backface culling
    ;; @todo:

    ;; if $y0 == $y1 then we can draw a single triangle and avoid division by zero
    (if (i32.or (i32.eq %y0 %y1) (i32.eq %y1 %y2))
        (then
            (if (i32.eq %y0 %y1)
                (then
                    (call $fill_partial_triangle ;; y0 and y1 are equal
                        %y2
                        %y0
                        %x2
                        %x1
                        %x0
                        %color
                    )
                    (return)
                )
                (else
                    (call $fill_partial_triangle ;; y1 and y2 are equal
                        %y0
                        %y1
                        %x0
                        %x1
                        %x2
                        %color
                    )
                    (return)
                )
            )
        )
    )

    (local.set $long_slope
        (f32.div
            (f32.convert_i32_s
                (i32.sub
                    %x2
                    %x0))
            (f32.convert_i32_s ;; positive
                (i32.sub
                    %y2
                    %y0))))

    (call $fill_partial_triangle
        %y0
        %y1
        %x0
        %x1
        (i32.add %x0 (i32.trunc_f32_s (f32.mul (f32.convert_i32_s (i32.sub %y1 %y0)) %long_slope)))
        %color
    )

    (call $fill_partial_triangle
        %y2
        %y1
        %x2
        %x1
        (i32.add %x0 (i32.trunc_f32_s (f32.mul (f32.convert_i32_s (i32.sub %y1 %y0)) %long_slope)))
        %color
    )
)


(export "draw_triangle_lines_2" (func $draw_triangle))
(func $draw_triangle
    (param $x0 i32) (param $y0 i32)
    (param $x1 i32) (param $y1 i32)
    (param $x2 i32) (param $y2 i32)
    (param $color i32)

    (local $x i32)
    (local $y i32)

    (local $segment_height i32)
    (local $total_height i32)
    (local $alpha f32)
    (local $beta f32)

    (local $a i32)
    (local $b i32)


    (if (i32.gt_s (local.get $y0) (local.get $y1))
        (then
            swap_local($x0, $x1)
            swap_local($y0, $y1)
        )
    )

    (if (i32.gt_s (local.get $y0) (local.get $y2))
        (then
            swap_local($x0, $x2)
            swap_local($y0, $y2)
        )
    )

    (if (i32.gt_s (local.get $y1) (local.get $y2))
        (then
            swap_local($x1, $x2)
            swap_local($y1, $y2)
        )
    )

    (local.set $total_height (i32.sub (local.get $y2) (local.get $y0)))

    (local.set $y (local.get $y0))
    (loop
        (local.set $segment_height
            (i32.add
                (i32.sub
                    (local.get $y1)
                    (local.get $y0))
                (i32.const 1)))

        (local.set $alpha
            (f32.div
                (f32.convert_i32_s (i32.sub (local.get $y) (local.get $y0)))
                (f32.convert_i32_s (local.get $total_height))
            )
        )
        (local.set $beta
            (f32.div
                (f32.convert_i32_s (i32.sub (local.get $y) (local.get $y0)))
                (f32.convert_i32_s (local.get $segment_height))
            )
        )

        (local.set $a
            (i32.trunc_f32_s
                (f32.add
                    (f32.convert_i32_s
                        (local.get $x0))
                    (f32.mul
                        (f32.convert_i32_s
                            (i32.sub
                                (local.get $x2)
                                (local.get $x0)))
                    (local.get $alpha)))))

        (local.set $b
            (i32.trunc_f32_s
                (f32.add
                    (f32.convert_i32_s
                        (local.get $x0))
                    (f32.mul
                        (f32.convert_i32_s
                            (i32.sub
                                (local.get $x1)
                                (local.get $x0)))
                    (local.get $beta)))))

        order_low_high($a, $b)

        (local.set $x (local.get $a))
        (loop
            set_pixel($x, $y, $color)

            (local.set $x (i32.add (local.get $x) (i32.const 1)))
            (br_if 0 (i32.le_s (local.get $x) (local.get $b)))
        )

        (local.set $y (i32.add (local.get $y) (i32.const 1)))
        (br_if 0 (i32.le_s (local.get $y) (local.get $y1)))
    )

    ;; Second triangle
    (local.set $y (local.get $y1))
    (loop
        (local.set $segment_height
            (i32.add
                (i32.sub
                    (local.get $y2)
                    (local.get $y1))
                (i32.const 1)))

        (local.set $alpha
            (f32.div
                (f32.convert_i32_s (i32.sub (local.get $y) (local.get $y0)))
                (f32.convert_i32_s (local.get $total_height))
            )
        )
        (local.set $beta
            (f32.div
                (f32.convert_i32_s (i32.sub (local.get $y) (local.get $y1)))
                (f32.convert_i32_s (local.get $segment_height))
            )
        )

        (local.set $a
            (i32.trunc_f32_s
                (f32.add
                    (f32.convert_i32_s
                        (local.get $x0))
                    (f32.mul
                        (f32.convert_i32_s
                            (i32.sub
                                (local.get $x2)
                                (local.get $x0)))
                    (local.get $alpha)))))

        (local.set $b
            (i32.trunc_f32_s
                (f32.add
                    (f32.convert_i32_s
                        (local.get $x1))
                    (f32.mul
                        (f32.convert_i32_s
                            (i32.sub
                                (local.get $x2)
                                (local.get $x1)))
                    (local.get $beta)))))

        order_low_high($a, $b)

        (local.set $x (local.get $a))
        (loop
            set_pixel($x, $y, $color)

            (local.set $x (i32.add (local.get $x) (i32.const 1)))
            (br_if 0 (i32.le_s (local.get $x) (local.get $b)))
        )

        (local.set $y (i32.add (local.get $y) (i32.const 1)))
        (br_if 0 (i32.le_s (local.get $y) (local.get $y2)))
    )
)

;; This method has been optimized as of this commit.
;; Optimizations include:
;; - Values arrive as f32s. There was never any usage of them as u8, which made them being ints unnecessary. (No performance impact, but less code)
;; - x/y in the loop are now cached as floats as to avoid exmra casts. (180ms -> 170ms per 1000x triangles)
;; - Refactored some instructions to avoid three exmra instances of i32.add and f32.neg (170ms -> 150ms per 1000x triangles)

;; This procedure currently takes 150ms to do the triangle test case
(export "draw_triangle" (func $draw_triangle_bruteforce_f32))
(func $draw_triangle_bruteforce_f32
    (param $x0 f32) (param $y0 f32)
    (param $x1 f32) (param $y1 f32)
    (param $x2 f32) (param $y2 f32)
    (param $color i32)

    (local $x i32)
    (local $y i32)

    ;; (local $x01 i32) (local $y01 i32)
    ;; (local $x02 i32) (local $y02 i32)

    (local $minx i32)
    (local $miny i32)
    (local $medx i32)
    (local $medy i32)
    (local $maxx i32)
    (local $maxy i32)

    ;; 0 = p0 -> p1 etc
    (local $nx0 f32) (local $ny0 f32) (local $o0 f32)
    (local $nx1 f32) (local $ny1 f32) (local $o1 f32)
    (local $nx2 f32) (local $ny2 f32) (local $o2 f32)

    (local $t0 f32)

    (local $fx f32)
    (local $fy f32)

    (local.set $minx (i32.trunc_f32_s %x0))
    (local.set $miny (i32.trunc_f32_s %y0))
    (local.set $medx (i32.trunc_f32_s %x1))
    (local.set $medy (i32.trunc_f32_s %y1))
    (local.set $maxx (i32.trunc_f32_s %x2))
    (local.set $maxy (i32.trunc_f32_s %y2))

    i32_fast_sort($minx, $medx, $maxx)
    i32_fast_sort($miny, $medy, $maxy)
    (local.set $minx clamp($minx, 0, 255))
    (local.set $maxx clamp($maxx, 0, 255))
    (local.set $miny clamp($miny, 0, 255))
    (local.set $maxy clamp($maxy, 0, 255))

    ;; (local.set $x01 (i32.sub %x1 %x0))
    ;; (local.set $y01 (i32.sub %y1 %y0))
    ;; (local.set $x02 (i32.sub %x2 %x0))
    ;; (local.set $y02 (i32.sub %y2 %y0))

    ;; What is going on here: WRONG WRONG WRONG @todo
    ;; For points 0->1, 1->2, 2->0:
    ;; we get the delta x and y, and then the distance between the two
    ;; Then we calculate the offset from the origin
    ;; Then we actually normalize x and y, and swizzle them into -y, x

    ;; @todo: Optimizations
    ;;  - SIMD
    (local.set $nx0 (f32.sub %x1 %x0))
    (local.set $ny0 (f32.sub %y1 %y0))
    (local.set $t0 (f32.sqrt (f32.add (f32.mul %nx0 %nx0) (f32.mul %ny0 %ny0))))
    (local.set $o0 (f32.div (f32.sub (f32.mul %nx0 %y0) (f32.mul %ny0 %x0)) %t0))
    (local.set $nx0 (f32.div %nx0 %t0))
    (local.set $ny0 (f32.div %ny0 %t0))
    swap_local($nx0, $ny0)
    (local.set $nx0 (f32.neg %nx0))

    (local.set $nx1 (f32.sub %x2 %x1))
    (local.set $ny1 (f32.sub %y2 %y1))
    (local.set $t0 (f32.sqrt (f32.add (f32.mul %nx1 %nx1) (f32.mul %ny1 %ny1))))
    (local.set $o1 (f32.div (f32.sub (f32.mul %nx1 %y1) (f32.mul %ny1 %x1)) %t0))
    (local.set $nx1 (f32.div %nx1 %t0))
    (local.set $ny1 (f32.div %ny1 %t0))
    swap_local($nx1, $ny1)
    (local.set $nx1 (f32.neg %nx1))

    (local.set $nx2 (f32.sub %x0 %x2))
    (local.set $ny2 (f32.sub %y0 %y2))
    (local.set $t0 (f32.sqrt (f32.add (f32.mul %nx2 %nx2) (f32.mul %ny2 %ny2))))
    (local.set $o2 (f32.div (f32.sub (f32.mul %nx2 %y2) (f32.mul %ny2 %x2)) %t0))
    (local.set $nx2 (f32.div %nx2 %t0))
    (local.set $ny2 (f32.div %ny2 %t0))
    swap_local($nx2, $ny2)
    (local.set $nx2 (f32.neg %nx2))

    (local.set $x (local.get $minx))
    (loop $loop_x
        (local.set $fx (f32.convert_i32_s %x))
        (local.set $y (local.get $miny))
        (loop $loop_y
            (local.set $fy (f32.convert_i32_s %y))
            (block $no_pixel
                (br_if $no_pixel
                    (f32.ge
                        (f32.add
                            (f32.mul %fx %nx0)
                            (f32.mul %fy %ny0)
                        )
                        %o0
                    )
                )

                (br_if $no_pixel
                    (f32.ge
                        (f32.add
                            (f32.mul %fx %nx1)
                            (f32.mul %fy %ny1)
                        )
                        %o1
                    )
                )

                (br_if $no_pixel
                    (f32.ge
                        (f32.add
                            (f32.mul %fx %nx2)
                            (f32.mul %fy %ny2)
                        )
                        %o2
                    )
                )

                ;; @todo: DDA
                (local.set $color (call $rgb_from_floats
                    (call $f32_barycentric
                        %x0 %y0 %x1 %y1 %x2 %y2 (f32.convert_i32_s %x) (f32.convert_i32_s %y)
                    )
                    (f32.const 0.0)
                ))

                set_pixel($x, $y, $color)
            )

            increment($y)
            (br_if $loop_y (i32.le_s (local.get $y) (local.get $maxy)))
        )
        increment($x)
        (br_if $loop_x (i32.le_s (local.get $x) (local.get $maxx)))
    )
)

;; Imagine lines connecting these. My attempts failed.
;; Also, $xl and $xr arent necessarily on different sides of $xm
;;
;;                 $xm,$y0
;;
;;
;;       $xl,$y1                $xr,$y1
;;
(func $fill_partial_triangle
    (param $y0 i32) (param $y1 i32)
    (param $xm i32) (param $xl i32) (param $xr i32) ;; x middle, left, right
    (param $color i32)

    (local $x i32)
    (local $y i32)
    (local $ys i32)

    (local $r i32)

    (local $ls f32) (local $rs f32) ;; left, right slope. x = xm + y * slope

    ;; @todo: Needed?
    (if (i32.lt_s (local.get $xr) (local.get $xl)) (then swap_local($xl, $xr)))

    (local.set $ys sign_from($y0, $y1))

    (local.set $ls (f32.div (f32.convert_i32_s (i32.sub %xl %xm)) (f32.convert_i32_s (i32.sub %y1 %y0))))
    (local.set $rs (f32.div (f32.convert_i32_s (i32.sub %xr %xm)) (f32.convert_i32_s (i32.sub %y1 %y0))))

    (local.set $y %y0)
    (loop $loop_y
        (local.set $x (i32.add %xm (i32.trunc_f32_s (f32.mul %ls (f32.convert_i32_s (i32.sub %y %y0))))))
        (local.set $r (i32.add %xm (i32.trunc_f32_s (f32.mul %rs (f32.convert_i32_s (i32.sub %y %y0))))))

        ;; (call $put_i (i32.sub %x %r))

        (loop $loop_x
            set_pixel($x, $y, $color)

            increment($x)
            (br_if $loop_x (i32.le_s %x %r))
        )

        increment_by($y, $ys)
        (if (i32.eq %ys (i32.const 1))
            (then (br_if $loop_y (i32.le_s %y %y1)))
            (else (br_if $loop_y (i32.ge_s %y %y1)))
        )
    )
)

(;
(export "draw_triangle" (func $draw_triangle))
(func $draw_triangle
    (param $x0 i32) (param $y0 i32)
    (param $x1 i32) (param $y1 i32)
    (param $x2 i32) (param $y2 i32)
    (param $color i32)

    (local $x i32)
    (local $y i32)

    (local $swap_lh i32)

    ;; Height
    (local $lh i32)
    (local $hh i32)

    ;; Ratio
    (local $lr i32)
    (local $hr i32)

    (local $rat f32)

    ;; Dist
    (local $ld i32)
    (local $hd i32)

    (local $l i32)
    (local $r i32)

    (local $segment_height i32)
    (local $total_height i32)

    (if (i32.gt_s (local.get $y0) (local.get $y1))
        (then
            swap_local($x0, $x1)
            swap_local($y0, $y1)
        )
    )

    (if (i32.gt_s (local.get $y0) (local.get $y2))
        (then
            swap_local($x0, $x2)
            swap_local($y0, $y2)
        )
    )

    (if (i32.gt_s (local.get $y1) (local.get $y2))
        (then
            swap_local($x1, $x2)
            swap_local($y1, $y2)
        )
    )

    ;; Left is the tall edge
    (local.set $lh (i32.sub (local.get $y1) (local.get $y0)))
    (local.set $hh (i32.sub (local.get $y2) (local.get $y0)))

    (local.set $swap_lh (i32.lt_s (local.get $hh) (local.get $lh)))

    (if (local.get $swap_lh) (then swap_local($lh, $hh)))

    ;; Note - not yet necessarily left/right
    (local.set $ld (i32.sub (local.get $x0) (local.get $x2)))
    (local.set $hd (i32.sub (local.get $x0) (local.get $x1)))

    (if (local.get $swap_lh) (then swap_local($ld, $hd)))

    ;; lr = (float)ld / (float)lh
    (local.set $lr (f32.div (f32.convert_i32_s (local.get $ld)) (f32.convert_i32_s (local.get $lh)))

    ;; rh / lh = what amount of the tall edge should be accounted for
    ;; rat = rh / lh
    (local.set $rat (f32.div (f32.convert_i32_s (local.get $rh)) (f32.convert_i32_s (local.get $lh))))

    ;; hr = (rd * rat) / lh;
    (local.set $hr
        (f32.div
            (f32.mul
                (f32.convert_i32_s (local.get $rd))
                (local.get $rat))
            (f32.convert_i32_s (local.get $lh))))

    (if (f32.lt (local.get $hr) (local.get $lr)) (then swap_local($hr, $lr)))

    (local.set $y (local.get $y0))
    (loop
        (local.set $l (i32.trunc_f32_s (f32.mul (f32.convert_i32_s (local.get $y)) (local.get $lr))))
        (local.set $r (i32.trunc_f32_s (f32.mul (f32.convert_i32_s (local.get $y)) (local.get $hr))))

        (local.set $x (local.get $l))
        (loop
            set_pixel($x, $y, $color)

            increment($x)
            (br_if 0 (i32.le_s (local.get $x) (local.get $r)))
        )
        ;; l = x0
        ;; (local.set $l ())
        increment($y)
        (br_if 0 (i32.lt_s (local.get $y) (local.get $y1)))
    )
)
;)
