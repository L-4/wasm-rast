#pragma once

#include "helpers.wat"

;; struct MemHeader {
;;      MemHeader* prev; // 0
;;      MemHeader* next; // 4
;;      uint32_t size; // 8
;;      uint8_t data[?]; // 12
;; }; // 12 + size

#define MH_PREV_OFS 0
#define MH_NEXT_OFS 4
#define MH_SIZE_OFS 8
#define MH_DATA_OFS 12

;; // The first 0x40000 bytes are used by the backbuffer, which conveniently fits four pages.
;; // The fifth page is free to use for any static data you'd like (overkill I know)
;; // The sixth page and onward is used in heap allocations

#define STATIC_MEM_START 0x40000

;; bool true = blend, false = no blend
#define SM_DRAW_BLENDMODE 0x40004
;; Font data is 0x200 bytes long (64 * 8)
#define SM_TEST_FONT 0x40008
#define SM_TEST_STR 0x40208
;; Note that string literals are null terminated
(data (i32.const SM_TEST_STR) "What's up?")

#define PTR_FIRST_MEM_HEADER 0x50000
#define PTR_LAST_MEM_HEADER 0x50004
#define HEAP_START 0x50008

#define mem_load_abs(OFS) (i32.load (i32.const OFS))

#define mem_load_base_ofs(BASE, OFS) (i32.load (i32.add (local.get BASE) (i32.const OFS)))
#define mem_store_base_ofsc(BASE, OFS, VAL) (i32.store (i32.add (local.get BASE) (i32.const OFS)) (i32.const VAL))
#define mem_store_base_ofsv(BASE, OFS, VAL) (i32.store (i32.add (local.get BASE) (i32.const OFS)) (local.get VAL))

#define mem_is_null(LOC) (i32.eq (i32.load LOC) (i32.const 0))

;; Create an allocation with zero size to act as the starting point for other allocations
(func $init_dummy_alloc
    (local $base i32)

    (i32.store (i32.const PTR_FIRST_MEM_HEADER) (i32.const HEAP_START))
    (i32.store (i32.const PTR_LAST_MEM_HEADER) (i32.const HEAP_START))

    (local.set $base (i32.const HEAP_START))

    mem_store_base_ofsc($base, MH_PREV_OFS, 0)
    mem_store_base_ofsc($base, MH_NEXT_OFS, 0)
    mem_store_base_ofsc($base, MH_SIZE_OFS, 0)
)

(func $get_heap_start_after (param $hdr i32) (result i32)
    i32.const MH_DATA_OFS
    mem_load_base_ofs($hdr, MH_SIZE_OFS) i32.add
    local.get $hdr i32.add
)

(func $grow_heap_to_fit (param $addr i32)
    ;; This is a bit confusing since we are operating on u32s
    ;; i32 simply means that we are operating on 32 bits with int semantics
    (i32.div_u (local.get $addr) (i32.const 0x10000))
    (memory.size)

    i32.sub ;; Now we are back to signed
    local.tee $addr

    i32.const 0
    i32.lt_s

    (if
    (then
        ;; i32.sub 0, $n negates $n
        (memory.grow (i32.sub (i32.const 0) (local.get $addr)))
        ;; memory.grow returns -1 if allocation failed. Warn in debug, else drop
#ifdef DEBUG
        i32.const -1
        i32.eq
        (if (then WARN()))
#else
        drop
#endif
    ))
)

;; Create allocation without trying to fit it between other allocations
(export "alloc_last" (func $alloc_last))
(func $alloc_last (param $size i32) (result i32)
    (local $prev_heap_loc i32)
    (local $this_heap_loc i32)

    (local.set $prev_heap_loc (i32.load (i32.const PTR_LAST_MEM_HEADER)))
    (local.tee $this_heap_loc (call $get_heap_start_after (local.get $prev_heap_loc)))

    ;; We need room up to &mem + sizeof(MemHeader) + header::size
    ;; &mem is already on stack
    i32.const 12 i32.add
    mem_load_base_ofs($this_heap_loc, MH_SIZE_OFS) i32.add

    call $grow_heap_to_fit
    ;; Initialize next allocation
    mem_store_base_ofsv($this_heap_loc, MH_PREV_OFS, $prev_heap_loc)
    mem_store_base_ofsc($this_heap_loc, MH_NEXT_OFS, 0)
    mem_store_base_ofsv($this_heap_loc, MH_SIZE_OFS, $size)

    ;; Point previous allocation to this
    mem_store_base_ofsv($prev_heap_loc, MH_NEXT_OFS, $this_heap_loc)

    (i32.store (i32.const PTR_LAST_MEM_HEADER) (local.get $this_heap_loc))

    local.get $this_heap_loc
)

;; Returns ofs+size for allocation DATA
(export "get_alloc_view" (func $get_alloc_view))
(func $get_alloc_view (param $alloc i32) (result i32) (result i32)
    (i32.add (local.get $alloc) (i32.const MH_DATA_OFS))
    mem_load_base_ofs($alloc, MH_SIZE_OFS)
)
