#pragma once

;; @todo: Make stuff conditional moves
#define i32_increment_local(LOCAL) (local.set LOCAL (i32.add (local.get LOCAL) (i32.const 1)))
#define WARN() (call $warn (i32.const __LINE__))
#define swap_local(L1, L2) local.get L1 local.get L2 local.set L1 local.set L2
#define order_low_high(L1, L2) (if (i32.gt_s (local.get L1) (local.get L2)) (then swap_local(L1, L2)))
#define increment(VAL) (local.set VAL (i32.add (local.get VAL) (i32.const 1)))
#define increment_by(VAL, BY) (local.set VAL (i32.add (local.get VAL) (local.get BY)))
#define decrement_by(VAL, BY) (local.set VAL (i32.sub (local.get VAL) (local.get BY)))
#define sign_from(L1, L2) (if (result i32) (i32.lt_s (local.get L1) (local.get L2)) (then (i32.const 1)) (else (i32.const -1)))
#define i32_min(L1, L2) (select (local.get L1) (local.get L2) (i32.lt_s (local.get L1) (local.get L2)))
#define i32_max(L1, L2) (select (local.get L1) (local.get L2) (i32.gt_s (local.get L1) (local.get L2)))
#define f32_min(L1, L2) (select (local.get L1) (local.get L2) (f32.lt (local.get L1) (local.get L2)))
#define f32_max(L1, L2) (select (local.get L1) (local.get L2) (f32.gt (local.get L1) (local.get L2)))

;; For now, this is just bubblesort (which is actually fast in this case)
;; L1 is loswest, L3 is highest. Values are written back to locals
#define i32_fast_sort(L1, L2, L3) \
    (if (i32.gt_s (local.get L1) (local.get L2)) (then swap_local(L1, L2))) \
    (if (i32.gt_s (local.get L1) (local.get L3)) (then swap_local(L1, L3))) \
    (if (i32.gt_s (local.get L2) (local.get L3)) (then swap_local(L2, L3)))

#define clamp(V, MIN, MAX) \
    (if (result i32) \
        (i32.lt_s (local.get V) (i32.const MIN)) \
        (then (i32.const MIN)) \
        (else \
            (if (result i32) \
                (i32.gt_s (local.get V) (i32.const MAX)) \
                (then (i32.const MAX)) \
                (else (local.get V)))))
