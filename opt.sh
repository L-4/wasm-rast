mkdir -p temp bin
cpp -E src/main.wat | sed -r 's/#/;;/' > temp/main.wat
./wabt/wat2wasm.exe temp/main.wat -o bin/main.wasm
./wabt/wasm-strip.exe bin/main.wasm
